import React, { useState } from 'react';

function ManufacturerForm({fetchManufacturers}) {

const [name, setName] = useState('')

const handleNameChange = (event) => {
    const value = event.target.value
    setName(value)
}

const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {}
    data.name = name;
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
    };
    const manufacturerResponse = await fetch(manufacturerUrl, fetchConfig);
    if (manufacturerResponse.ok) {
        const newManufacturer = await manufacturerResponse.json();
        fetchManufacturers();
    
        setName('')
    const successTag = document.getElementById('success-message')
    successTag.classList.remove('d-none');
    }
}
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new manufacturer</h1>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                <div className="alert alert-success d-none mb-0" id="success-message">
                    Successfully created Manufacturer!
                </div>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;