import React, { useState } from 'react';

function VehicleModelForm({manufacturers, fetchVehicleModels, fetchManufacturers}) {

const [name, setName] = useState('')
const [picture, setPicture] = useState('')
const [manufacturer, setManufacturer] = useState('')

const handleNameChange = (event) => {
    const value = event.target.value
    setName(value)
}

const handlePictureChange = (event) => {
    const value = event.target.value
    setPicture(value)
}

const handleManufacturerChange = (event) => {
    const value = event.target.value
    setManufacturer(value)
}

const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {}
    data.name = name;
    data.picture_url = picture;
    data.manufacturer_id = manufacturer;
    const modelUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
    };
    const modelResponse = await fetch(modelUrl, fetchConfig);
    if (modelResponse.ok) {
        const newModel = await modelResponse.json();
        fetchVehicleModels();
        fetchManufacturers();

    setName('')
    setPicture('')
    setManufacturer('')
    const successTag = document.getElementById('success-message')
    successTag.classList.remove('d-none');
    }
}
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a Vehicle Model</h1>
                <form onSubmit={handleSubmit} id="create-vehiclemodel-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} value={picture} placeholder="Picture" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleManufacturerChange} value={manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                        <option value="">Choose a manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return (
                            <option key={manufacturer.id} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                            );
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                <div className="alert alert-success d-none mb-0" id="success-message">
                    Successfully created Vehicle!
                </div>
                </div>
            </div>
        </div>
    );
}

export default VehicleModelForm;