import React from 'react'

function AppointmentList({appointments, fetchAppointments}) {

  const cancelAppointment = async (appointment) => {
    const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/`;
    const fetchConfig = {
        method: "delete",
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(appointmentUrl, fetchConfig)
    if (response.ok) {
      fetchAppointments()
    }
  }
    

  const finishAppointment = async (appointment) => {
    const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/`;
    const fetchConfig = {
        method: "put",
        body: JSON.stringify({ finished: true }),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(appointmentUrl, fetchConfig)
    if (response.ok) {
      fetchAppointments()
    }
  }
   
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Date/Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>VIP</th>
              <th>Finished</th>
            </tr>
          </thead>
          <tbody>
            {appointments.map(appointment => {
              return (
                <tr key={appointment.id}>
                  <td>{ appointment.vin }</td>
                  <td>{ appointment.customer_name }</td>
                  <td>{ new Date(appointment.date_time).toLocaleString("en-US") }</td>
                  <td>{ appointment.technician }</td>
                  <td>{ appointment.reason }</td>
                  <td>{ appointment.vip ? "Yes" : "No"}</td>
                  <td>{ appointment.finished ? "Yes" : "No"}</td>
                  <td>
                    <button className="btn btn-danger" onClick={() => cancelAppointment(appointment)}>Cancel</button>
                    <button className="btn btn-success" onClick={() => finishAppointment(appointment)}>Finished</button>
                  </td>   
                </tr>
              );
            })}
          </tbody>
        </table>
    )
}
export default AppointmentList