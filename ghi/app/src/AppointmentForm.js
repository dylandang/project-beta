import React, { useState } from 'react';

function AppointmentForm({technicians, fetchAppointments}) { 
    const [vin, setVin] = useState('')
    const [customerName, setCustomerName] = useState('')
    const [dateTime, setDateTime] = useState('')
    const [technician, setTechnician] = useState('')
    const [reason, setReason] = useState('')
    
    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }
    const handleCustomerNameChange = (event) => {
        const value = event.target.value
        setCustomerName(value)
    }
    const handleDateTimeChange = (event) => {
        const value = event.target.value
        setDateTime(value)
    }
    const handleTechnicianChange = (event) => {
        const value = event.target.value
        setTechnician(value)
    }
    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.vin = vin;
        data.customer_name = customerName;
        data.date_time = dateTime;
        data.technician = technician;
        data.reason = reason;
        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const appointmentResponse = await fetch(appointmentUrl, fetchConfig);
        if (appointmentResponse.ok) {
            const newAppointment = await appointmentResponse.json();
            fetchAppointments();
        
        setVin('')
        setCustomerName('')
        setDateTime('')
        setTechnician('')
        setReason('')
        const successTag = document.getElementById('success-message')
        successTag.classList.remove('d-none');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create an appointment</h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control"/>
                    <label htmlFor="vin">Vin</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleCustomerNameChange} value={customerName} placeholder="Customer Name" required type="text" name="customer_name" id="customer_name" className="form-control"/>
                    <label htmlFor="customer_name">Customer Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleDateTimeChange} value={dateTime} placeholder="Date/Time" required type="text" name="date_time" id="date_time" className="form-control"/>
                    <label htmlFor="date_time">Date/Time</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
                    <label htmlFor="reason">Reason</label>
                    </div>
                    <div className="form-floating mb-3">
                    </div>
                    <div className="mb-3">
                    <select onChange={handleTechnicianChange} value={technician} required name="technician" id="technician" className="form-select">
                        <option value="">Choose a technician</option>
                        {technicians.map(technician => {
                            return (
                            <option key={technician.id} value={technician.id}>
                                {technician.name}
                            </option>
                            );
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                <div className="alert alert-success d-none mb-0" id="success-message">
                    Successfully created Appointment!
                </div>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm;