import React, { useState } from 'react';

function SalesRecordForm({automobile, customers, salesPersonz, fetchAutomobile, fetchCustomer, fetchSalesPersons, fetchSalesRecord}) {
    const [automobilerec, setAutomobileRec] = useState('')
    const [salesPersons, setSalesPersons] = useState('')
    const [customer, setCustomer] = useState('')
    const [price, setPrice] = useState('')

    const handleAutomobileChange = (event) => {
        const value = event.target.value
        setAutomobileRec(value)
    }
    const handleSalesPersonsChange = (event) => {
        const value = event.target.value
        setSalesPersons(value)
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }
    const handlePriceChange = (event) => {
        const value = event.target.value
        setPrice(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.automobile = automobilerec;
        data.sales_persons = salesPersons;
        data.customer = customer;
        data.price = price;

        const salesRecordUrl = "http://localhost:8090/api/sales_record/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const salesRecordResponse = await fetch(salesRecordUrl, fetchConfig);
        if (salesRecordResponse.ok) {
            const newSalesRecord = await salesRecordResponse.json();
            fetchAutomobile();
            fetchCustomer();
            fetchSalesPersons();
            fetchSalesRecord();


        setAutomobileRec('')
        setSalesPersons('')
        setCustomer('')
        setPrice('')
        }
    }


    return (
        <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new sales record</h1>
                <form onSubmit={handleSubmit} id="create-sales_record-form">
                    <div className="mb-3">
                        <select onChange={handleAutomobileChange} value={automobilerec} id="automobile" required name="automobile" className="form-select">
                            <option value="">Choose an automobile</option>
                            {automobile.map(automobile => {
                                return (
                                <option key={automobile.href} value={automobile.href}>
                                    {automobile.vin}
                                </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleSalesPersonsChange} value={salesPersons} required name="salesPersons" id="salesPersons" className="form-select">
                            <option value="">Choose a sales person</option>
                            {salesPersonz.map(worker => {
                                return (
                                <option key={worker.id} value={worker.name}>
                                    {worker.name}
                                </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                            <option value="">Choose a customer</option>
                            {customers.map(buyer => {
                                return (
                                <option key={buyer.id} value={buyer.name}>
                                    {buyer.name}
                                </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePriceChange} value={price} placeholder="Price" required type="text" name="price" id="price" className="form-control"/>
                        <label htmlFor="price">Price</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
        </>
    );
}

export default SalesRecordForm;
