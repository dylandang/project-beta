import React from 'react'

function SalesRecordList({salesRecords}) {
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Person</th>
              <th>Employee Number</th>
              <th>Customer</th>
              <th>Automobile Vin</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {salesRecords.map(salesRecord => {
              return (
                <tr key={salesRecord.id}>
                  <td>{ salesRecord.sales_persons.name }</td>
                  <td>{ salesRecord.sales_persons.employee_number }</td>
                  <td>{ salesRecord.customer }</td>
                  <td>{ salesRecord.automobile}</td>
                  <td>{ salesRecord.price }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
    )
}
export default SalesRecordList
