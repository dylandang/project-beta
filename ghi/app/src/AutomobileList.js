import React from 'react'

function AutomobileList({automobiles}) {
    return (
        <>
        <h1>Vehicle Models</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Color</th>
              <th>Year</th>
              <th>Model</th>
              <th>Manufacturer</th>
            </tr>
          </thead>
          <tbody>
            {automobiles.map(auto => {
              return (
                <tr key={auto.vin}>
                  <td>{ auto.vin }</td>
                  <td>{ auto.color }</td>
                  <td>{ auto.year }</td>
                  <td>{ auto.model.name}</td>
                  <td>{ auto.model.manufacturer.name }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
    )
}
export default AutomobileList
