import React, { useEffect, useState} from 'react'

function AppointmentHistory() {
    const [appointments, setAppointments] = useState([])
    const [search, setSearch] = useState('')

    const handleSearch = async (event) => {
        const value = event.target.value
        setSearch(value)
    }
    async function fetchAppointments() {
        const url = "http://localhost:8080/api/appointments"
        const response = await fetch(url)
        const data = await response.json()
        const appointmentData = data.appointments
        const result = appointmentData.filter(appointment => appointment.vin === search)
        setAppointments(result)
    }
    useEffect(() => {
        fetchAppointments();
    },[])
    
    return (
        <>
        <div className="input-group">
            <input onChange={handleSearch} type="text" value={search} className="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
            <button type="button" onClick={fetchAppointments} className="btn btn-light">Search VIN</button>
        </div>
            <h1>Service Appointments</h1>
                <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date/Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>VIP</th>
                        <th>Finished</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                    return (
                        <tr key={appointment.id}>
                        <td>{ appointment.vin }</td>
                        <td>{ appointment.customer_name }</td>
                        <td>{ new Date(appointment.date_time).toLocaleString("en-US") }</td>
                        <td>{ appointment.technician }</td>
                        <td>{ appointment.reason }</td> 
                        <td>{ appointment.vip ? "Yes" : "No"}</td>
                        <td>{ appointment.finished ? "Yes" : "No"}</td>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
        </>
    )
}
export default AppointmentHistory