import React, { useState } from 'react';

function SalesPersonsForm({fetchSalesPersons}) {
    const [name, setName] = useState('')
    const [employeeNumber, setEmployeeNumber] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value
        setEmployeeNumber(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.name = name;
        data.employee_number = employeeNumber;
        const salesPersonsUrl = "http://localhost:8090/api/sales_persons/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const salesPersonsResponse = await fetch(salesPersonsUrl, fetchConfig);
        if (salesPersonsResponse.ok) {
            const newSalesPersons = await salesPersonsResponse.json();
            fetchSalesPersons();

        setName('')
        setEmployeeNumber('')
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a sales person</h1>
                <form onSubmit={handleSubmit} id="create-salespersons-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeNumberChange} value={employeeNumber} placeholder="Employee number" required type="text" name="employee_number" id="employee_number" className="form-control"/>
                        <label htmlFor="employee_number">Employee number</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
    );
}

export default SalesPersonsForm;
