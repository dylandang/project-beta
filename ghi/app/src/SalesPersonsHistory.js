import React, { useState} from 'react'

function SalesPersonsHistory({salesPersonz}) {
    const [salesRecords, setSalesRecord] = useState([])
    const [select, setSelect] = useState('')

    const handleSelect = async (event) => {
        const value = event.target.value
        setSelect(value)
        fetchSalesRecord()
    }

    async function fetchSalesRecord() {
        const url = `http://localhost:8090/api/sales_record/`
        const response = await fetch(url)
        const data = await response.json()
        const salesRecordData = data.sales_persons
        const result = salesRecordData.filter(sales_persons => sales_persons.employee_number === select)

        setSalesRecord(result)
    }
    // // useEffect(() => {
    // //     fetchSalesRecord();
    // // },[])
    return (
        <>
            <h1>Sales Person History</h1>
            <h2>In progress and coming soon!</h2>
                <div className="mb-3">
                    <select onChange={handleSelect} onSelect={fetchSalesRecord} value={select} required name="salesPerson" id="salesPersons" className="form-select">
                        <option value="">Choose a sales person</option>
                            {salesPersonz.map(salesPersons => {
                                return (
                                <option key={salesPersons.id} value={salesPersons.employee_number}>
                                    {salesPersons.name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales person</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salesRecords.map(salesRecord => {
                        return (
                            <tr key={salesRecord.id}>
                                <td>{ salesRecord.sales_persons.name }</td>
                                <td>{ salesRecord.customer }</td>
                                <td>{ salesRecord.automobile}</td>
                                <td>{ salesRecord.price }</td>
                            </tr>
                    );
                    })}
                </tbody>
                </table>
        </>
    )
}
export default SalesPersonsHistory;
