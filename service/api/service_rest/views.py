from django.shortcuts import render
from common.json import ModelEncoder
from .models import Appointment, AutomobileVO, Technician
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

class TechnicianVOEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "color",
        "year",
        "vin",
        "import_href",
        "id",
    ]

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",       
        "date_time",     
        "reason",  
        "vip",
        "finished",
        "cancelled",
        "id",
    ]
    encoders = {
        "technician": TechnicianVOEncoder,
    }

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",       
        "date_time",     
        "technician", 
        "reason",  
        "vip",
        "finished",
        "cancelled",
        "id",
    ]

    def get_extra_data(self, o):
        return {"technician": o.technician.name}

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            TechnicianVOEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technicians = Technician.objects.create(**content)
            return JsonResponse(
                technicians,
                TechnicianVOEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_technicians(request, pk):
    if request.method == "GET":
        technicians = Technician.objects.get(id=pk)
        return JsonResponse(
            technicians,
            TechnicianVOEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0},
        )
    else:
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technicians = Technician.objects.get(id=content["technician"])
                content["technician"] = technicians
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )
        Technician.objects.filter(id=pk).update(**content)
        technicians = Technician.objects.get(id=pk)
        return JsonResponse(
            technicians,
            TechnicianVOEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        technicians = Technician.objects.get(id=content["technician"])
        content["technician"] = technicians
        try:
            AutomobileVO.objects.get(vin=content["vin"])
            content["vip"] = True
        except AutomobileVO.DoesNotExist: 
            content["vip"] = False
    appointments = Appointment.objects.create(**content)
    return JsonResponse(
        appointments,
        AppointmentListEncoder,
        safe=False,
    )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointments(request, pk):
    if request.method == "GET":
        appointments = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointments,
            AppointmentListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            if "appointments" in content:
                appointments = Appointment.objects.get(id=content["appointments"])
                content["appointments"] = appointments
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"}
            )
        Appointment.objects.filter(id=pk).update(**content)
        appointments = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointments,
            AppointmentListEncoder,
            safe=False,
        )
