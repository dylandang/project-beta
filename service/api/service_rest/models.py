from django.db import models

class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.vin
        
class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=50)
    date_time = models.DateTimeField(auto_now_add=True)
    reason = models.CharField(max_length=50)
    vip = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    cancelled = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    
    
    
    


    