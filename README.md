# CarCar

**Team:**
- Automobile Service - Dylan Dang
- Automobile Sales - Christian Hanna

**Design:**
- Backend: Django to create RESTful APIs in microservices.
- Frontend: React to create a front-end application that uses the RESTful APIs.
- Database: PostgreSQL will hold the data of all of the microservices.

**Steps to clone and run application:**
- clone the repository of this link in the terminal: https://gitlab.com/dylandang/project-beta
	- REMEMBER TO CHANGE TO THE PROJECT DIRECTORY ONCE CLONED.
	- `cd project-beta`
- download insomnia and docker desktop
- run these commands in the terminal:
	- `docker volume create beta-data`
	- `docker-compose build`
	- `docker-compose up`
	- THIS MAY TAKE A FEW MINUTES FOR SERVERS and REACT TO FULLY START UP. 
	- ONCE SERVERS ARE FULLY RUNNING, GO TO [localhost:3000](url) ON YOUR BROWSER TO USE THE WEBSITE.
- IN OUR BROWSER, WHEN CREATING NEW AUTOMOBILE, IT TAKES A LITTLE TIME TO SHOW ONTO SALESRECORD DROPDOWN FOR AUTOMOBILE VINS. WOULD SHOW UP AFTER 10 SECONDS AND A REFRESH IS NEEDED.

**Context Map:**
![IMAGE](/images/DIAGRAM.PNG)

## Inventory microserice http://localhost:8100
- GET request to /api/automobiles/
![IMAGE](/images/GET%20AUTOMOBILE.PNG)
- POST request to /api/automobiles/
![IMAGE](/images/POST%20AUTOMOBILE.PNG)
- GET request to /api/manufacturers/
![IMAGE](/images/GET%20MANUFACTURER.PNG)
- POST request to /api/manufacturers/
![IMAGE](/images/POST%20MANUFACTURER.PNG)
- GET request to /api/models/
![IMAGE](/images/GET%20VEHICLEMODELS.PNG)
- POST request to /api/models/
![IMAGE](/images/POST%20VEHICLEMODELS.PNG)

## Service microservice http://localhost:8080
- GET request to /api/appointments/
![IMAGE](/images/GET%20APPOINTMENTS.PNG)
- POST request to /api/appointments/
![IMAGE](/images/POST%20APPOINTMENTS.PNG)
- GET request to /api/technicians/
![IMAGE](/images/GET%20TECHNICIANS.PNG)
- POST request to /api/technicians/
![IMAGE](/images/POST%20TECHNICIAN.PNG)

## Explain your models and integration with the inventory microservice, here.
- Service microservice contains 3 models. AutomobileVO, which has 2 attributes: vin and import_href. Technician, which has 2 attributes: name and employee_number. Appointment, which has 8 attributes: vin, customer_name, date_time, reason, vip, finished, cancelled and technician as a foreignkey. The value object for service microservice is AutomobileVO, and from that we capture the VIN, since it cannot be changed and VIN uniquely identifies a car. The VIN is captured from the automobile model in inventory through poller.py. If the customer purchased the car from the dealership and brings their car in for a maintenance, it will show that their car is a VIP. Users are able to create new technicians, see a list of all technicians, schedule an appointment for maintenance, see their existing appointment and see the history of maintenance done on their vehicle using their VIN. 

## Sales microservice http://localhost:8090
- GET request to /api/customer/
![IMAGE](/images/Get%20customers.png)
- POST request to /api/customer/
![IMAGE](/images/Create%20customer.png)
- GET request to /api/sales_record/
![IMAGE](/images/Get%20sales%20record.png)
- POST request to /api/sales_record/
![IMAGE](/images/Create%20sales%20record.png)
- GET request to /api/sales_persons/
![IMAGE](/images/Get%20sales%20persons.png)
- POST request to /api/sales_persons/
![IMAGE](/images/Post%20sales%20persons.png)

## Explain your models and integration with the inventory microservice, here.
- The sales microservice has four models. The models are AutomobileVO, Sales_persons, Customer, and Sales_record. AutomobileVO contains the attributes vin, year, color, import_href, and availability. Sales_persons model contains attributes name and employee_number, and Customer contains name, address, and phone_number. The last model, Sales_record, contains attributes that involve data from other models,so they are retrieved via foreign keys. Three foreign key attributes are sales_persons, customers, and automobile, and the last attribute in Sales_records, price, does not use a foreign key. The value object, AutomobileVO, is taken from the inventory microservice using a poller. This data does not originate in the sales microservice. The most important data here is vin which has the VIN number of the automobiles. This doesn't change and it unique to the automobiles. Polling automobile data allows the microservice to handle it's own version of the data. Availability information is added in the sales microservice to the AutomobileVO model to determine whether the automobile can be sold. If availability = True it can be sold, and if availability = False it cannot be sold. With the addition of the sales microservice users are able to create customers, sales people, and sales records. That information is then kept in a database. Listing sales history by a sales person is not functioning in our project. The problem was that after selecting a sales person in the drop down menu their own sales would not list. I could either get all the sales of every sales person or just no sales. My thinking process was to filter the sales records by the sales person’s name and map the sales records if it is the same as a sales person's name.
