from django.shortcuts import render
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .models import Sales_persons, Customer, Sales_record, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
        "year",
        "color",
        "availability"
    ]

class Sales_personsEncoder(ModelEncoder):
    model = Sales_persons
    properties = [
        "name",
        "employee_number",
        "id"
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]

class Sales_recordEncoder(ModelEncoder):
    model = Sales_record
    properties = [
        "sales_persons",
        "automobile",
        "customer",
        "price",
        "id",
    ]
    def get_extra_data(self, o):
        return {
                "automobile": o.automobile.vin,
                "sales_persons": {
                    "name": o.sales_persons.name,
                    "employee_number": o.sales_persons.employee_number,
                },
                "customer": o.customer.name
        }
    encoders = {
        "sales_persons": Sales_personsEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_sales_persons(request):
    if request.method == "GET":
        sales_persons = Sales_persons.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=Sales_personsEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            sales_persons = Sales_persons.objects.create(**content)
            return JsonResponse(
                sales_persons,
                encoder=Sales_personsEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Invalid salespersons"},
                status = 400
            )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_sales_persons(request, pk):
    if request.method == "GET":
        try:
            sales_persons = Sales_persons.objects.get(id=pk)
            return JsonResponse(
                sales_persons,
                Sales_personsEncoder,
                safe=False,
            )
        except Sales_persons.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        count, _ = Sales_persons.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0},
        )
    else:
        content = json.loads(request.body)
        try:
            if "sales_persons" in content:
                sales_persons = Sales_persons.objects.get(id=content["sales_persons"])
                content["sales_persons"] = sales_persons
        except Sales_persons.DoesNotExist:
                return JsonResponse(
                    {"message": "Does not exist"},
                    status=404,
                )
        Sales_persons.objects.filter(id=pk).update(**content)
        sales_persons = Sales_persons.objects.get(id=pk)
        return JsonResponse(
            sales_persons,
            Sales_personsEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Invalid, could not create the customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        count, _= Customer.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0},
        )
    else:
        content = json.loads(request.body)
        try:
            if "customer" in content:
                customer = Customer.objects.get(id=content["customer"])
                content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404,
            )
        Customer.objects.filter(id=pk).update(**content)
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_list_automobile(request):
    if request.method == "GET":
        automobile = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobile": automobile},
            encoder=AutomobileVOEncoder,
        )

@require_http_methods(["GET", "POST"])
def api_list_sales_record(request):
    if request.method == "GET":
        sales_record = Sales_record.objects.all()
        return JsonResponse(
            {"sales_record": sales_record},
            encoder=Sales_recordEncoder,
        )
    else:
        content = json.loads(request.body)
        try:

            car_name = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=car_name)
            if automobile.availability is True:
                content["automobile"] = automobile

                customer_name = content["customer"]
                customer = Customer.objects.get(name=customer_name)
                content["customer"] = customer

                sales_persons = content["sales_persons"]
                salespersons = Sales_persons.objects.get(name=sales_persons)
                content["sales_persons"] = salespersons

                automobile.availability = False
                automobile.save()

                record = Sales_record.objects.create(**content)
                return JsonResponse(
                    record,
                    encoder=Sales_recordEncoder,
                    safe=False,
                )
            else:
                response = JsonResponse(
                    {"message": "Sorry, this car is not available."}
                )
                response.status_code=400
                return response
        except:
            response = JsonResponse(
                {"message": "Could not create sale"}
            )
            response.status_code=400
            return response

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_sales_record(request, pk):
    if request.method == "GET":
        try:
            sales_record = Sales_record.objects.get(id=pk)
            return JsonResponse(
                sales_record,
                encoder=Sales_recordEncoder,
                safe=False,
            )
        except Sales_record.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        count, _= Sales_record.objects.filter(id=pk)
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            sales_record = Sales_record.objects.get(id=pk)

            props = ["sales_persons", "customer", "automobile", "price"]
            for prop in props:
                if prop in content:
                    setattr(sales_record, prop, content[prop])

            sales_record.save()
            return JsonResponse(
                sales_record,
                encoder=Sales_recordEncoder,
                safe=False
            )

        except Sales_record.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
