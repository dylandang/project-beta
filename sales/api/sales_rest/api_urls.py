from django.urls import path

from sales_rest.views import (
    api_list_sales_persons,
    api_list_customer,
    api_list_automobile,
    api_list_sales_record,
    api_show_customer,
    api_show_sales_persons,
    api_show_sales_record,
)

urlpatterns = (
    path("sales_persons/", api_list_sales_persons, name="api_list_sales_persons"),
    path("sales_persons/<int:pk>/", api_show_sales_persons, name="api_show_sales_persons"),
    path("customer/", api_list_customer, name="api_list_customer"),
    path("customer/<int:pk>/", api_show_customer, name="api_show_customer"),
    path("automobile/", api_list_automobile, name="api_list_automobile"),
    path("sales_record/", api_list_sales_record, name="api_list_sales_record"),
    path("sales_record/<int:pk>/", api_show_sales_record, name="api_list_show_record"),


)
