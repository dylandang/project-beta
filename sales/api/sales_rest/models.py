from django.db import models


class Sales_persons(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    year = models.PositiveBigIntegerField()
    color = models.CharField(max_length=50)
    import_href = models.CharField(max_length=200, unique=True)
    availability = models.BooleanField(default=True)

    def __str__(self):
        return self.vin

class Sales_record(models.Model):
    price = models.PositiveIntegerField(null=True)
    sales_persons = models.ForeignKey(
        Sales_persons,
        related_name = "sales",
        on_delete=models.PROTECT
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name = "sales",
        on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        Customer,
        related_name= "sales",
        on_delete=models.PROTECT
    )
    def __str__(self):
        return f"{self.sales_persons} {self.automobile} {self.customer}"
